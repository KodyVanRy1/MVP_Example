package com.owletcare.mvpexample

import android.app.Application

/**
 * ${FILE_NAME}
 * MVPExample
 *
 * Created by kvanry on 3/8/18.
 * Copyright (c) 2018. Desitum. All rights reserved worldwide.
 */
class MvpApp: Application() {

    override fun onCreate() {
        super.onCreate()
        app = this
    }

    companion object {
        lateinit var app: MvpApp
    }

}