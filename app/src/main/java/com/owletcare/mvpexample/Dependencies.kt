package com.owletcare.mvpexample

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context

@SuppressLint("StaticFieldLeak")
/**
 * ${FILE_NAME}
 * MVPExample
 *
 * Created by kvanry on 3/8/18.
 * Copyright (c) 2018. Desitum. All rights reserved worldwide.
 */
object Dependencies {

    private val dbOfMagicInteractor: MagicInteractor by lazy { MagicInteraction(MvpApp.app) }

    @JvmStatic
    fun inject(activity: Activity) {
        when (activity) {
            is MainActivity -> {
                val coordinator = MainActivityCoordination(activity)
                val presenter = MainActivityPresentation(dbOfMagicInteractor)
                activity.provide(coordinator, presenter)
            }
        }
    }

}