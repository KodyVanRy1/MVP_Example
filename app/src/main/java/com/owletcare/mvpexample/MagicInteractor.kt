package com.owletcare.mvpexample

import android.content.Context

/**
 * ${FILE_NAME}
 * MVPExample
 *
 * Created by kvanry on 3/8/18.
 * Copyright (c) 2018. Desitum. All rights reserved worldwide.
 */
interface MagicInteractor {
    fun getSandwich(onSandwichResult: (String) -> Unit)

}

class MagicInteraction(val context: Context) : MagicInteractor {

    override fun getSandwich(onSandwichResult: (String) -> Unit) {
        val sandwichResult = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                .getString(SANDWICH_NAME, DEFAULT_SANDWICH)
        onSandwichResult(sandwichResult)
    }

    companion object {
        const val SANDWICH_NAME = "sandwich_name"
        const val DEFAULT_SANDWICH = "Poof,You're a sandwich"
    }

}