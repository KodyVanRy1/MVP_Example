package com.owletcare.mvpexample

/**
 * ${FILE_NAME}
 * MVPExample
 *
 * Created by kvanry on 3/8/18.
 * Copyright (c) 2018. Desitum. All rights reserved worldwide.
 */
interface MainActivityPresenter {
    fun makeMeASandwich(onSandwichResult: (Sandwich) -> Unit)
    fun getSandwichFromSandwichResult(sandwichResult: String): Sandwich
}

class MainActivityPresentation(private val interactor: MagicInteractor) : MainActivityPresenter {

    override fun makeMeASandwich(onSandwichResult: (Sandwich) -> Unit) {
        interactor.getSandwich { sandwichResult ->
            onSandwichResult(getSandwichFromSandwichResult(sandwichResult))
        }
    }

    override fun getSandwichFromSandwichResult(sandwichResult: String): Sandwich {
        val results = sandwichResult.split(",")
        return Sandwich(results[0], results[1])
    }

}