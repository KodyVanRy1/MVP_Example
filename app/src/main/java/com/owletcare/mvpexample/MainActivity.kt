package com.owletcare.mvpexample

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var coordinator: MainActivityCoordinator
    lateinit var presenter: MainActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        Dependencies.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadDefaultSandwich()
        makeMeASandwichButton.setOnClickListener { makeMeASandwich() }
    }

    fun provide(coordinator: MainActivityCoordinator, presenter: MainActivityPresenter) {
        this.coordinator = coordinator
        this.presenter = presenter
    }

    private fun loadDefaultSandwich() {
        val sandwich = Sandwich(intent.getStringExtra(DEFAULT_SANDWICH_NAME), intent.getStringExtra(DEFAULT_SANDWICH_DESCRIPTION))
        currentStateTextView.text = "$sandwich"
    }

    private fun makeMeASandwich() {
        presenter.makeMeASandwich { sandwich ->
            currentStateTextView.text = "$sandwich"
        }
    }

    companion object {
        private const val DEFAULT_SANDWICH_NAME = "default_sandwich_name"
        private const val DEFAULT_SANDWICH_DESCRIPTION = "default_sandwich_description"
        @JvmStatic
        fun getIntent(context: Context, defaultSandwich: Sandwich) {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(DEFAULT_SANDWICH_NAME, defaultSandwich.name)
            intent.putExtra(DEFAULT_SANDWICH_DESCRIPTION, defaultSandwich.description)
        }
    }
}
