package com.owletcare.mvpexample

/**
 * ${FILE_NAME}
 * MVPExample
 *
 * Created by kvanry on 3/8/18.
 * Copyright (c) 2018. Desitum. All rights reserved worldwide.
 */
interface MainActivityCoordinator {

}

class MainActivityCoordination(val activity: MainActivity): MainActivityCoordinator {

}