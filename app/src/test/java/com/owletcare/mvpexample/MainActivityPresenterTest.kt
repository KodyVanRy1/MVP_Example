package com.owletcare.mvpexample

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class MainActivityPresenterTest {
    @Test
    fun sandwichParsing_isCorrect() {
        val presenter = MainActivityPresentation(getMagicInteractor())
        val sandwich = presenter.getSandwichFromSandwichResult("Poof,You're a sandwich")
        assertEquals("Poof", sandwich.name)
        assertEquals("You're a sandwich", sandwich.description)
    }

    private fun getMagicInteractor(): MagicInteractor {
        return object : MagicInteractor {
            override fun getSandwich(onSandwichResult: (String) -> Unit) {
                onSandwichResult("Poof,You're a sandwich")
            }
        }
    }
}
